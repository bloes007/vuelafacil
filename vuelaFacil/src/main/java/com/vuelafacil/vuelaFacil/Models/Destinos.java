package com.vuelafacil.vuelafacil.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import java.util.Set;
import java.util.List;

@Table
@Entity(name="destinos")
public class Destinos implements Serializable{
    @Id
    @Column(name="codigo_des")
    private String codigo_des;
    @Column(name="nombre_des")
    private String nombre_des;

    public Destinos(String codigo_des, String nombre_des) {
        this.codigo_des = codigo_des;
        this.nombre_des = nombre_des;
    }

    public Destinos() {
    }

    public String getCodigo_des() {
        return codigo_des;
    }

    public void setCodigo_des(String codigo_des) {
        this.codigo_des = codigo_des;
    }

    public String getNombre_des() {
        return nombre_des;
    }

    public void setNombre_des(String nombre_des) {
        this.nombre_des = nombre_des;
    }

}
