package com.vuelafacil.vuelafacil.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import java.util.Set;
import java.util.List;

@Table
@Entity(name="usuario")
public class Usuario implements Serializable{
    @Id
    @Column(name="documento_usu")
    private String documento_usu;
    @Column(name="nombre_usu")
    private String nombre_usu;
    @Column(name="edad_usu")
    private String edad_usu;
    @Column(name="genero_usu")
    private String genero_usu;
    @Column(name="nacionalidad_usu")
    private String nacionalidad_usu; 

    public Usuario(String documento_usu, String nombre_usu, String edad_usu, String genero_usu, String nacionalidad_usu) {
        this.documento_usu = documento_usu;
        this.nombre_usu = nombre_usu;
        this.edad_usu = edad_usu;
        this.genero_usu = genero_usu;
        this.nacionalidad_usu = nacionalidad_usu;
    }

    public Usuario() {
    }

    public String getDocumento_usu() {
        return documento_usu;
    }

    public void setDocumento_usu(String documento_usu) {
        this.documento_usu = documento_usu;
    }

    public String getNombre_usu() {
        return nombre_usu;
    }

    public void setNombre_usu(String nombre_usu) {
        this.nombre_usu = nombre_usu;
    }

    public String getEdad_usu() {
        return edad_usu;
    }

    public void setEdad_usu(String edad_usu) {
        this.edad_usu = edad_usu;
    }

    public String getGenero_usu() {
        return genero_usu;
    }

    public void setGenero_usu(String genero_usu) {
        this.genero_usu = genero_usu;
    }

    public String getNacionalidad_usu() {
        return nacionalidad_usu;
    }

    public void setNacionalidad_usu(String nacionalidad_usu) {
        this.nacionalidad_usu = nacionalidad_usu;
    }

}
