package com.vuelafacil.vuelafacil.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import java.util.Set;
import java.util.List;


@Table
@Entity(name="vuelo")
public class Vuelo implements Serializable{
    @Id
    @Column(name="numeroe_vuelo")
    private String numero_vuelo; 
    @Column(name="fecha_vuelo")
    private String fecha_vuelo;
    @Column(name="hora_vuelo")
    private String hora_vuelo; 

    public Vuelo(String numero_vuelo, String fecha_vuelo, String hora_vuelo) {
        this.numero_vuelo = numero_vuelo;
        this.fecha_vuelo = fecha_vuelo;
        this.hora_vuelo = hora_vuelo;
    }

    public Vuelo() {
    }

    public String getNumero_vuelo() {
        return numero_vuelo;
    }

    public void setNumero_vuelo(String numero_vuelo) {
        this.numero_vuelo = numero_vuelo;
    }

    public String getFecha_vuelo() {
        return fecha_vuelo;
    }

    public void setFecha_vuelo(String fecha_vuelo) {
        this.fecha_vuelo = fecha_vuelo;
    }

    public String getHora_vuelo() {
        return hora_vuelo;
    }

    public void setHora_vuelo(String hora_vuelo) {
        this.hora_vuelo = hora_vuelo;
    }
    
    

}
