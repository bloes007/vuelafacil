
package com.vuelafacil.vuelafacil.Dao;
import com.vuelafacil.vuelafacil.Models.Destinos;
import org.springframework.data.repository.CrudRepository; 

public interface DestinosDao extends CrudRepository<Destinos, String>{
    
}
