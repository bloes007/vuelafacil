
package com.vuelafacil.vuelafacil.Dao;
import com.vuelafacil.vuelafacil.Models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioDao extends CrudRepository<Usuario, String>{
    
}
