package com.vuelafacil.vuelafacil.Dao;

import com.vuelafacil.vuelafacil.Models.Vuelo;
import org.springframework.data.repository.CrudRepository;

public interface VueloDao extends CrudRepository<Vuelo, String>{

}
