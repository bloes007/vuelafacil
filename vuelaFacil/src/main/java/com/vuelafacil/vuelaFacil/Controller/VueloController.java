package com.vuelafacil.vuelafacil.Controller;

import com.vuelafacil.vuelafacil.Models.Vuelo;
import com.vuelafacil.vuelafacil.Service.VueloService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/vuelo")
public class VueloController {

    @Autowired
    VueloService vueloService;

    @PostMapping(value = "/")
    public ResponseEntity<Vuelo> agregar(@RequestBody Vuelo vuelo) {
        Vuelo obj = vueloService.save(vuelo);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/id")
    public ResponseEntity<Vuelo> eliminar(@PathVariable String id) {
      Vuelo obj = vueloService.findById(id);
        if (obj != null) {
            vueloService.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Vuelo> editar(@RequestBody Vuelo vuelo) {
        Vuelo obj = vueloService.findById(vuelo.getNumero_vuelo());
        if (obj != null) {
            obj.setFecha_vuelo(vuelo.getFecha_vuelo());
            obj.setHora_vuelo(vuelo.getHora_vuelo());
            vueloService.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Vuelo> consultarTodo() {
        return vueloService.findbyAll();
    }

    @GetMapping("/list/{id}")
    public Vuelo consultarPorId(@PathVariable String id) {
        return vueloService.findById(id);

    }

}


