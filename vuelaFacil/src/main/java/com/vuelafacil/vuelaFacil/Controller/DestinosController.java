
package com.vuelafacil.vuelafacil.Controller;
import com.vuelafacil.vuelafacil.Models.Destinos;
import com.vuelafacil.vuelafacil.Service.DestinosService;
import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/destinos")
public class DestinosController {
    @Autowired
    DestinosService destinosService; 
    
     @PostMapping(value = "/")
    public ResponseEntity<Destinos> agregar(@RequestBody Destinos destinos){
        Destinos obj = destinosService.save(destinos);
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }
    
    @DeleteMapping(value = "/id")
    public ResponseEntity<Destinos> eliminar(@PathVariable String id){
        Destinos obj = destinosService.findById(id);
        if(obj!=null)
            destinosService.delete(id);
        else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value = "/")
    public ResponseEntity<Destinos> editar(@RequestBody Destinos destinos){
        Destinos obj = destinosService.findById(destinos.getCodigo_des());
        if(obj!=null) {
            obj.setNombre_des(destinos.getNombre_des());
            destinosService.save(obj); 
        }
        else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }
    
    @GetMapping("/list")
    public List<Destinos> consultarTodo(){
        return destinosService.findbyAll();
    }
    
    @GetMapping("/list/{id}")
    public Destinos consultarPorId(@PathVariable String id){
        return destinosService.findById(id); 
}
    
}