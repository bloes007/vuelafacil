
package com.vuelafacil.vuelafacil.Service.Implement;
import com.vuelafacil.vuelafacil.Models.Vuelo;
import com.vuelafacil.vuelafacil.Service.VueloService;
import com.vuelafacil.vuelafacil.Dao.VueloDao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VueloServiceImpl implements VueloService{
    @Autowired
    private VueloDao vueloDao; 

    @Override
    @Transactional(readOnly = false)
    public Vuelo save(Vuelo vuelo) {
        return vueloDao.save(vuelo); 
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(String id) {
        vueloDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Vuelo findById(String id) {
        return vueloDao.findById(id).orElse(null); 
    }

    @Override
    @Transactional(readOnly = true)
    public List<Vuelo> findbyAll() {
        return (List<Vuelo>) vueloDao.findAll(); 
    }
}
