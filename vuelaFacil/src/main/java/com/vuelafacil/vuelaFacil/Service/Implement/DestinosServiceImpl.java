package com.vuelafacil.vuelafacil.Service.Implement;

import com.vuelafacil.vuelafacil.Models.Destinos;
import com.vuelafacil.vuelafacil.Service.DestinosService;
import com.vuelafacil.vuelafacil.Dao.DestinosDao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DestinosServiceImpl implements DestinosService {
    @Autowired
    private DestinosDao destinosDao;

    @Override
    @Transactional(readOnly = false)
    public Destinos save(Destinos destinos) {
        return destinosDao.save(destinos); 
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(String id) {
        destinosDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Destinos findById(String id) {
        return destinosDao.findById(id).orElse(null); 
    }

    @Override
    @Transactional(readOnly = true)
    public List<Destinos> findbyAll() {
        return (List<Destinos>) destinosDao.findAll();
    }
}
