package com.vuelafacil.vuelafacil.Service;

import com.vuelafacil.vuelafacil.Models.Destinos;
import java.util.List;

public interface DestinosService {

    public Destinos save(Destinos destinos);

    public void delete(String id);

    public Destinos findById(String id);

    public List<Destinos> findbyAll();

}
