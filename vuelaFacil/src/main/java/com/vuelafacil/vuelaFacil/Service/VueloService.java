package com.vuelafacil.vuelafacil.Service;

import com.vuelafacil.vuelafacil.Models.Vuelo;
import java.util.List;

public interface VueloService {
    public Vuelo save(Vuelo vuelo);

    public void delete(String id);

    public Vuelo findById(String id);

    public List<Vuelo> findbyAll();

}
