package com.vuelafacil.vuelafacil.Service;

import com.vuelafacil.vuelafacil.Models.Usuario;
import java.util.List;

public interface UsuarioService {

    public Usuario save(Usuario usuario);

    public void delete(String id);

    public Usuario findById(String id);

    public List<Usuario> findbyAll();

}
